# Crachá

Local | Total - Unidade (R$) 
----- | ---------- 
A     | 625,00 - 2,50
B     | 0,00     
C     | 352,00 - 1,40
D     | 126,00 - 0,50

## Descrição do Crachá

- Unidades: 250
- Tipo: Crachá de papel Couchê 150g
- Dimensões: 10cm x 14cm Couchê 150g
- Especificação: 4x0 cores

## Local

### A

 * Nome: Figi Gráfica
 * Contato: (82) 99991-1119

### B

 * Nome: Eloh Print
 * Contato: (82) 99123-1351

### C

 * Nome: Casa das Cópias Gbarbosa
 * Contato: (82) 9150-8207

### D

 * Nome: Gráfica Papel Impresso
 * Contato: (82) 98815-9761
 * Nota: 170g à R$ 189,00 e 115g à R$ 126,00
