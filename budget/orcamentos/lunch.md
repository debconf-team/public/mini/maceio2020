# Almoço


Local | Individual (R$) | Self-service | Distância (KM) | Total (R$)
----- | --------------  | ------------ | -------------- | ----------
A     | 69,00           | Sim          | 1              | 6900,00
B     | 44,00           | Sim          | 1.7            | 4480,00
C     | 38,90           | Sim          | 1              | 3890,00
D     | 25,00           | Sim          | 1              | 2500,00

## Descrição

- Quantidade de dias: 5
- Quantidade de pessoas: 20
- Incluir, se possível, opção vegetariana ou vegana

## Local A

 * Nome: Casa de Mainha
 * Endereço: R. Sá e Albuquerque, 417 - Jaraguá, Maceió - AL, 57022-290
 * Telefone: (82) 98138-2897
 * Site: restaurantecasademainha.com
 * Nota: Buffet livre com sobremesa, suco e água. Com refrigerante fica 74.90

## Local B

 * Nome: Fogão Colonial e Hamburgueria
 * Telefone: 82991142259
 * Endereço: Av. Dr. Antônio Gouveia, 981 - Pajuçara, Maceió - AL, 57030-170
 * Nota: Almoço e Janta. ALMOÇO das 12:00 às 16:00 e JANTA das 17:00 às 22:00
 * A empresa fogão colonial e hamburgueria, vem através dessa carta informa que os valores das confraternizações dos messes novembro e dezembro para almoço e janta são:
   * Pacote 1: 89,99 (buffet completo com mais de 60 opções diversas + churrasco no almoço e cardápio do melhor sanduiche da cidade e diversos petiscos no jantar + SOBREMESA + BEBIDAS NÃO ALCOOLICAS (AGUAS, SUCOS, REFRIGERANTES EM JARRA, CAFÉ E CAPUCCIONO)
   * Pacote 2: 119,99  (buffet completo com mais de 60 opções diversas + churrasco no almoço e cardápio do melhor sanduiche da cidade e diversos petiscos no jantar + SOBREMESA + BEBIDAS ALCOOLISCAS E NÃO ALCOOLICAS (AGUAS, SUCOS, REFRIGERANTES EM JARRA, CERVEJA, CACHAÇA, CAFÉ E CAPUCCIONO)

## Local C

 * Nome: Toca do Calango
 * Site:
 * Endereço: R. Melo Póvoas, 258-B - Jaraguá, Maceió - AL, 57022-230
 * Nota: 01 (UMA) BEBIDA (água, refrigerante, suco) E 01(UM) PRATO PRINCIPAL

## Local D

 * Nome: Mercado Jaraguá
 * Site:
 * Endereço:
 * Nota: Várias opções

