# Jantar


Local | Individual (R$) | Self-service | Distância (KM) | Total (R$) 
----- | --------------  | ------------ | -------------- | -----
A     | 44,80           | Sim          | 1.7            | 4480,00 
B     |                 |              |                |
C     |                 |              |                |
D     |                 |              |                |

## Descrição

- Quantidade de dias: 5
- Quantidade de pessoas: 20
- Incluir, se possível, opção vegetariana ou vegana

## Local A

  * Nome: Fogão Colonial e Hamburgueria
 * Telefone: 82991142259
 * Endereço: Av. Dr. Antônio Gouveia, 981 - Pajuçara, Maceió - AL, 57030-170
 * Nota: Almoço e Janta. ALMOÇO das 12:00 às 16:00 e JANTA das 17:00 às 22:00
 * A empresa fogão colonial e hamburgueria, vem através dessa carta informa que os valores das confraternizações dos messes novembro e dezembro para almoço e janta são:
Pacote 1: 89,99 (buffet completo com mais de 60 opções diversas + churrasco no almoço e cardápio do melhor sanduiche da cidade e diversos petiscos no jantar + SOBREMESA + BEBIDAS NÃO ALCOOLICAS (AGUAS, SUCOS, REFRIGERANTES EM JARRA, CAFÉ E CAPUCCIONO)
Pacote 2: 119,99  (buffet completo com mais de 60 opções diversas + churrasco no almoço e cardápio do melhor sanduiche da cidade e diversos petiscos no jantar + SOBREMESA + BEBIDAS ALCOOLISCAS E NÃO ALCOOLICAS (AGUAS, SUCOS, REFRIGERANTES EM JARRA, CERVEJA, CACHAÇA, CAFÉ E CAPUCCIONO)

## Local B

 * Nome:
 * Site:
 * Endereço:
 * Nota:

## Local C

 * Nome:
 * Site:
 * Endereço:
 * Nota:

## Local D

 * Nome:
 * Site:
 * Endereço:
 * Nota:

