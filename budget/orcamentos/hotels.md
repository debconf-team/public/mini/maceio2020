# Hoteis Maceió


Local | Duplo (R$) | Disponíveis | Auditório | Café  | Buffet (R$) | Distância (KM)
----- | ---------  | ----------- | --------- | ----- | ----------- | --------------
Maceió Mar Hotel     | 558,25     | Sim         | Sim       | Sim   | 0           | 3.8
Ibis Maceió Pajuçara     | 380        | Sim         | Não       | Sim   | 48          | 1.2
Matsubara Hotel    | 310        | Não         | Não       | Sim   | 0           | 6.8
Hotel Conde    | 230        | Sim         | Não       | Sim   | 0           | 6.2 
Ibis Budget Maceió Pajuçara     | 230        | Sim         | Não       | sim   | 0           | 0.9
Ritz Praia Hotel     | 440        | Sim         | Não       | sim   | 0           | 3.6

## Maceió Mar Hotel

 * Nome: Maceió Mar Hotel
 * Endereço: Av. Álvaro Otacílio, 2991 - Ponta Verde, Maceió - AL, 57035-180
 * Telefone: (82) 2122-8000
 * Nota: 10 quartos para 2 pessoas com 5 diárias fica R$ 27.912,50

## Ibis Maceió Pajuçara

 * Nome: Ibis Maceió Pajuçara
 * Endereço: Av. Dr. Antônio Gouveia, 277 - Pajuçara, Maceió - AL, 57030-170
 * Telefone: (82) 2121-6699
 * Nota: 10 quartos para 2 pessoas com 5 diárias fica R$ 19.000,00

## Matsubara Hotel

 * Nome: Matsubara Hotel
 * Endereço: Av. Brigadeiro Eduardo Gomes de Brito, 1551 - Lagoa da Anta, Maceió - AL, 57038-230
 * Telefone: (82) 3214-3000
 * Nota: Indisponível na data solicitada

## Hotel Conde

 * Nome: Hotel Conde
 * Endereço: R. Ouvidor Mendonça, 111 - Cruz das Almas, Maceió - AL, 57038-380
 * Telefone: (82) 3325-1947
 * Nota: 10 quartos para 2 pessoas com 5 diárias fica R$ 11.500,00

## Ibis Budget Maceió Pajuçara

 * Nome: Ibis Budget Maceió Pajuçara
 * Endereço: R. Epaminondas Gracindo, 210 - Pajuçara, Maceió - AL, 57030-100
 * Telefone: (82) 3024-2128
 * Nota: 10 quartos para 2 pessoas com 5 diárias fica R$ 11.500,00

## Ritz Praia Hotel

 * Nome: Ritz Praia Hotel
 * Endereço: R. Eng. Mario de Gusmão, 1300 - Ponta Verde, Maceió - AL, 57035-000
 * Telefone: (82) 99125-6825
 * Nota: 10 quartos para 2 pessoas com 5 diárias fica R$ 22.250,00

