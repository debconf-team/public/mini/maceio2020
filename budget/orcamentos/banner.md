# Banners

Local | Total (R$) | B1 - Uni. (R$)  | B2 - Uni. (R$)  | B3 - Uni. (R$) | B4 - Uni. (R$) | B5 - Uni. (R$)
----- | ---------- | ----------------| ----------------| -------------- | -------------- | ---------------
A     | 810,00     | 200,00 - 40,00  | 120,00 - 60,00  | 60,00 - 30,00  | 30,00 - 30,00  | 400,00 - 400,00
B     | 560,00     | 350,00 - 70,00  | 100,00 - 50,00  | 80,00 - 40,00  | 30,00 - 30,00  |  
C     | 1.477,00   | 470,00 - 94,00  | 139,60 - 69,80  | 78,00 - 39,00  | 70,00 - 70,00  | 720,00 - 720,00
D     | 825,00     | 220,00 - 44,00  | 130,00 - 65,00  | 50,00 - 25,00  | 25,00 - 25,00  | 400,00 - 400,00

## Descrição dos Banners (B1 à B5)

- B1: Espaço -  5 unidades - 0,70x1,00 m
- B2: Palco - 2 unidades - 0,90x1,20 m
- B3: Porta do Lab - 2 unidades - 0,50x0,70 m
- B4: Mesa do Palco - 1 unidade - Mesa no palco - 0,50x0,70 m
- B5: Backdrop - 1 unidade - 2,00x3,00 m

## Local

### A

 * Nome: Figi Gráfica
 * Contato: (82) 99991-1119

### B

 * Nome: Eloh Print
 * Contato: (82) 99123-1351

### C

 * Nome: Casa das Cópias Gbarbosa
 * Contato: (82) 9150-8207

### D

 * Nome: Gráfica Papel Impresso
 * Contato: (82) 98815 9761
 * Nota: Desconto com apoio e material com ilhoes.